#Web Server Re-arrange Order, cannot come first.

DISM /online /enable-feature /featurename:IIS-WebServer

#Common HTTP Features

DISM /online /enable-feature /featurename:IIS-CommonHttpFeatures
DISM /online /enable-feature /featurename:IIS-StaticContent
DISM /online /enable-feature /featurename:IIS-DefaultDocument
DISM /online /enable-feature /featurename:IIS-DirectoryBrowsing
DISM /online /enable-feature /featurename:IIS-HttpErrors

#Application Development

DISM /online /enable-feature /featurename:IIS-ApplicationDevelopment
DISM /online /enable-feature /featurename:IIS-ISAPIExtensions
DISM /online /enable-feature /featurename:IIS-ISAPIFilter
DISM /online /enable-feature /featurename:IIS-ASPNET
DISM /online /enable-feature /featurename:IIS-NetFxExtensibility
DISM /online /enable-feature /featurename:IIS-ASP
DISM /online /enable-feature /featurename:IIS-CGI

#Security

DISM /online /enable-feature /featurename:IIS-Security
DISM /online /enable-feature /featurename:IIS-BasicAuthentication
DISM /online /enable-feature /featurename:IIS-WindowsAuthentication
DISM /online /enable-feature /featurename:IIS-RequestFiltering

#Performance

DISM /online /enable-feature /featurename:IIS-Performance
DISM /online /enable-feature /featurename:IIS-HttpCompressionStatic

#Management Toots

DISM /online /enable-feature /featurename:IIS-WebServerManagementTools
DISM /online /enable-feature /featurename:IIS-ManagementConsole
DISM /online /enable-feature /featurename:IIS-ManagementScriptingTools
DISM /online /enable-feature /featurename:IIS-ManagementService
