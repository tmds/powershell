# This script parses an outlook inbox for  all conversations containing conversationIDs equal or greater than a count of 4 and outputs them into a .csv file
function Support(){
	$path = 'C:\Directory\AppDirectory'
	$date = get-date
	$num_month = get-date -uformat "%m"
	$time_stamp = $date.toString("MMdy")
	$year = $date.toString("yyyy")
	$month = (Get-Culture).DateTimeFormat.GetAbbreviatedMonthName(($date).month)
	$day = $date.toString("dd")
	$hour = $date.toString("%h")
	$minute = $date.toString("%m")

	Add-type -assembly "Microsoft.Office.Interop.Outlook" | out-null 
	$olFolders = "Microsoft.Office.Interop.Outlook.olDefaultFolders" -as [type]  
	$outlook = new-object -ComObject outlook.application 
	$namespace = $outlook.GetNameSpace("MAPI") 
	$folder = $namespace.getDefaultFolder($olFolders::olFolderInBox) 

	$support = $folder.items | Select-Object -Property ReceivedTime, ConversationID, ConversationTopic, SenderName, SenderEmailAddress | ?{$_.ReceivedTime.Year -eq (Get-Date).Year -and $_.ReceivedTime.Month -eq (Get-Date).Month}
	$conversation_group = ($support | Group-Object -property{$_.ConversationID} | ?{$_.Count -ge 4})
	$conversation_group = $conversation_group | select -ExpandProperty Name
	New-Item -Type Directory -Force -Path "$path\$year\$num_month-$month"
	$header = "Total`tSubject`tStart_Date`tEnd_Date`tDuration (day:hr:min:sec)`tSender Name`tSender Email" | out-file -append "$path\$year\$num_month-$month\Email_Analysis_$time_stamp.csv"

	foreach ($item in $conversation_group){
		$body = ''
		$temp_conversation = ($support | ?{$_.ConversationID -eq $item}) 
		$subject = $temp_conversation | Select -ExpandProperty "ConversationTopic" | Select-Object -First 1
		$sender = $temp_conversation | Select -ExpandProperty "SenderName" | Select-Object -First 1
		$sender_email = $temp_conversation | Select -ExpandProperty "SenderEmailAddress" | Select-Object -First 1
		$total = $temp_conversation.count | Sort-Object count
		$start_date = $temp_conversation | Select -ExpandProperty "ReceivedTime" | Select-Object -First 1
		$end_date = $temp_conversation | Select -ExpandProperty "ReceivedTime" | Select-Object -Last 1	
		$duration = New-Timespan -start $start_date -end $end_date
		$convo_days = $duration | Select -Expandproperty Days
		$convo_hours = $duration | Select -Expandproperty Hours
		$convo_minutes = $duration | Select -Expandproperty Minutes
		$convo_seconds = $duration | Select -Expandproperty Seconds
		$body ="$total`t$subject`t$start_date`t$end_date`t$convo_days"+":"+"$convo_hours"+":"+"$convo_minutes"+":"+"$convo_seconds`t$sender`t$sender_email"
		$body | Out-File -append "$path\$year\$num_month-$month\Email_Analysis_$time_stamp.csv"
	}
}
