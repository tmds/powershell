$ErrorView = "NormalView"
$runner = ($env:USERNAME)

function logon_report_user($user){
	try{
	
		$min = (Get-Date '05:30')
		$max = (Get-Date '23:30')
		$csv = Import-CSV ".\report_log.csv"
		
		$csv_filtered = $csv | ?{($_.LastLogon-as [datetime]) -ge $min -and ($_.LastLogon -as [datetime]) -le $max -and ($_.LastLogon -NotContains("has not logged on yet.")) -and ($_.SamAccountName -eq $user)}
		$curr = [datetime]::ParseExact($csv_filtered[$csv_filtered.count-1].runat, "yyyyMMddTHHmmssffffZ", $null)
		$curr = $curr.ToUniversalTime()
		write-host "`nInitializing Logon Report for ${user} on ${curr}." -f Green
		
		$csv_filtered| Foreach-Object {$_.LastLogon = [DateTime]$_.LastLogon; $_} | Group-Object SamAccountName |
		Foreach-Object {
					#Create new object per server per day with max-values
					New-Object -TypeName psobject -Property ([ordered]@{
					SamAccountName = $_.Group[0].SamAccountName
					"First Logon" = $_.Group.LastLogon | Measure-Object -Minimum | Select-Object -ExpandProperty Minimum			
					"Last Logon" = $_.Group.LastLogon | Measure-Object -Maximum | Select-Object -ExpandProperty Maximum
					"OrgUnit" = $_.Group[0].OrgUnit
					}) 
		 }
	return 0
	}
	catch{
		write-host $Error[0] -f Red
		return 1
	}
}

<#
Find organizational groups for a specific user.
#>
function user_groups($user){
	([adsisearcher]"(samaccountname=$user)").FindOne().Properties.memberof
}

<#
Find groups members for a specific organizational unit.
#>
function group_members($ou, $criteria){
	@(Get-ADGroupMember $ou -Recursive).samaccountname | ?{$_ -like "*${criteria}*"} | Sort-Object -Unique
}

<#
Compares two AD users using a left join, outputting a list of groups each does not belong to.
#>
function compare_user_groups($user1, $user2){
	try{
		$group1 = ([adsisearcher]"(samaccountname=$user1)").FindOne().Properties.memberof
		
		$group2 = ([adsisearcher]"(samaccountname=$user2)").FindOne().Properties.memberof
		
		$diff1 = $group1| ?{$group2 -NotContains $_}
		
		$diff2 = $group2| ?{$group1 -NotContains $_}

		write-host "${user1} belongs to the following groups which ${user2} DOES NOT belong to." -f blue
		$diff1
		
		write-host "${user2} belongs to the following groups which ${user1} DOES NOT belong to." -f blue
		$diff2
	}
	catch{
		write-host $Error[0] -f Red
		return 1
	}
}

<#
This outputs a daily logon report for the provided Organizational Unit
#>
function logon_report_ou($ou){
	try{
		$min = (Get-Date '06:30')
		$max = (Get-Date '21:30')
		$csv = Import-CSV ".\report_log.csv"
		
		$csv_filtered = $csv | ?{($_.LastLogon-as [datetime]) -ge $min -and ($_.LastLogon -as [datetime]) -le $max -and ($_.LastLogon -NotContains("has not logged on yet.")) -and ($_.OrgUnit -eq $ou)}
		$curr = [datetime]::ParseExact($csv_filtered[$csv_filtered.count-1].runat, "yyyyMMddTHHmmssffffZ", $null)
		$curr = $curr.ToUniversalTime()
		write-host "`nInitializing Logon Report for ${ou} on ${curr}." -f Green
		
		$csv_filtered| Foreach-Object {$_.LastLogon = [DateTime]$_.LastLogon; $_} | Group-Object SamAccountName |
		Foreach-Object {
					#Create new object per server per day with max-values
					New-Object -TypeName psobject -Property ([ordered]@{
					SamAccountName = $_.Group[0].SamAccountName
					"First Logon" = $_.Group.LastLogon | Measure-Object -Minimum | Select-Object -ExpandProperty Minimum			
					"Last Logon" = $_.Group.LastLogon | Measure-Object -Maximum | Select-Object -ExpandProperty Maximum
					"OrgUnit" = $_.Group[0].OrgUnit
					}) 
		 }
	return 0
	}
	catch{
		write-host $Error[0] -f Red
		return 1
	}

}

<#
Generates a report for a specific user based on daily log on time.
#>
function generate_report_user($user){
	try{
		$timestamp = Get-Date -Format 'yyyyMMddTHHmmssffffZ'
		$dcs = Get-ADDomainController -Filter {Name -like "*"}
		$time = 0
		$a = @()
			foreach($dc in $dcs){ 
			$hostname = $dc.HostName
			$currentUser = Get-ADUser $user | Get-ADObject -Server $hostname -Properties lastLogon
			$time = $currentUser.LastLogon
			$dt = [DateTime]::FromFileTime($time)
				if($computerName = (Get-WmiObject Win32_ComputerSystem).Name -eq "SERVER"){
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = $dt
						DomainController = $dc
						RunAt = $timestamp
						RunBy = "admin"
					}
				}
				elseif($computerName = (Get-WmiObject Win32_ComputerSystem).Name -ne "SERVER"){
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = $dt
						DomainController = $dc
						RunAt = $timestamp
						RunBy = "${runner}"
					}	
				}
				else{
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = "has not logged on yet."
						DomainController = "N/A"
						RunAt = $timestamp
						RunBy = "${runner}"
					}
				}
			}
			($a | Sort-Object -Property LastLogon | Select-Object -Last 1 -Property SamAccountName, LastLogon, DomainController, RunAt, OrgUnit, RunBy) | Export-Csv ".\report_log.csv" -NoTypeInformation -Append
			$time = 0
		return 0
	}
	catch{
		write-host $Error[0] -f Red
		return 1
	}
}

<#
Generates a report for all members of an Organizational Unit.
#>
function generate_report_ou($ou){
	try{
		$timestamp = Get-Date -Format 'yyyyMMddTHHmmssffffZ'
		$dcs = Get-ADDomainController -Filter {Name -like "*"}
		$group = @(Get-ADGroupMember $ou -Recursive).samaccountname | Sort-Object -Unique
		$time = 0
		foreach($user in $group){
		$a = @()
			foreach($dc in $dcs){ 
			$hostname = $dc.HostName
			$currentUser = Get-ADUser $user | Get-ADObject -Server $hostname -Properties lastLogon
			$time = $currentUser.LastLogon
			$dt = [DateTime]::FromFileTime($time)
				if($computerName = (Get-WmiObject Win32_ComputerSystem).Name -eq "SERVER"){
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = $dt
						DomainController = $dc
						RunAt = $timestamp
						RunBy = "admin"
					}
				}
				elseif($computerName = (Get-WmiObject Win32_ComputerSystem).Name -ne "SERVER"){
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = $dt
						DomainController = $dc
						RunAt = $timestamp
						RunBy = "${runner}"
					}	
				}
				else{
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = "has not logged on yet."
						DomainController = "N/A"
						RunAt = $timestamp
						RunBy = "${runner}"
					}
				}
			}
			($a | Sort-Object -Property LastLogon | Select-Object -Last 1 -Property SamAccountName, LastLogon, DomainController, RunAt, OrgUnit, RunBy) | Export-Csv ".\report_log.csv" -NoTypeInformation -Append
			$time = 0
		}
		return 0
	}
	catch{
		write-host $Error[0] -f Red
		return 1
	}
}

<#
Generates a silent report.
#>
function _report($ou){
	try{
		$timestamp = Get-Date -Format 'yyyyMMddTHHmmssffffZ'
		$dcs = Get-ADDomainController -Filter {Name -like "*"}
		$group = @(Get-ADGroupMember $ou -Recursive).samaccountname | Sort-Object -Unique
		$time = 0
		$min = (Get-Date '00:00')
		$max = (Get-Date '23:59')
		$b = @()
		foreach($user in $group){
		$a = @()
			foreach($dc in $dcs){ 
			$hostname = $dc.HostName
			$currentUser = Get-ADUser $user | Get-ADObject -Server $hostname -Properties lastLogon
			$time = $currentUser.LastLogon
			$dt = [DateTime]::FromFileTime($time)
				if($dt -ge $min -and $dt -le $max){
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = $dt
						DomainController = $dc
						RunAt = $timestamp
						RunBy = "admin"
					}
				}
				else{
					$a += [pscustomobject]@{
						OrgUnit = $ou
						SamAccountName = $user
						LastLogon = "has not logged on yet."
						DomainController = "N/A"
						RunAt = $timestamp
						RunBy = "admin"
					}
				}
			}
			($a | Sort-Object -Property LastLogon | Select-Object -First 1 -Property SamAccountName, LastLogon, DomainController, RunAt, OrgUnit, RunBy) | Export-Csv ".\eventlog.csv" -NoTypeInformation -Append
			$time = 0
		}
	return 0
	}
	catch{
		write-host $Error[0] -f Red
		return 1
	}
}

<#
This function checks all logins which occur in the morning periodically.
#>
function am_check($ou){
	do{
		logon_report_ou $ou
		sleep 1800 | out-null
	}
	while(((get-date) -ge(get-date '06:30')) -and ((get-date) -le (get-date '09:30')))
	write-host "All done for the morning, enjoy the rest of your morning ${runner}!" -f Green
}

<#
Functions which generate data feed for reports every 30 minutes.
#>

function data_feed($ou){
	while($true){
		_report $ou
		sleep 1800 | out-null
	}
}

function data_feed_ou($ou){
	while($true){
		generate_report_ou $ou
		sleep 1800 | out-null
	}
}

function data_feed_user($user){
	while($true){
		generate_report_user $user
		sleep 1800 | out-null
	}
}