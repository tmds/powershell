$ErrorActionPreference= 'SilentlyContinue'
#directory to search for files to be sorted
$dir = "G:\"

#get all files, fullpathnames, creationtimes, and extensions
$list = Get-ChildItem $dir -Recurse -Force | Where-Object {$_.PSIsContainer -eq 'True'} | Select-Object PSIsContainer, FullName -First 3
$picture = ".gif",".jpg", "jpeg", ".png", ".svg", ".heic", ".tiff", ".heif", "avif", ".bmp",".webm"
$video = ".mp4", ".mpeg4", ".mov",".wvm",".avi",".mkv",".mpeg-4"
$document = ".pdf",".doc",".docx",".xls",".xlsx",".pptx",".ppt"

#creating directories to store media types for sorting
function createDir()
{
#create parent directories to store file types
$dirs = "pics","vids","docs"
    foreach($folder in $dirs)
    {
        New-Item -Path $dir -Name $folder -ItemType "directory" -Force
    }
}


function sortDir(){
    foreach ($item in $list){
        $fullname = $item.FullName
        write-host Processing directory: $fullname
        $files = Get-ChildItem $fullname -Recurse -Force | Select-Object FullName, Extension, CreationTime
        
        foreach($file in $files){
            $extension = $file.Extension
            $year = $file.CreationTime.ToString('yyyy')
            $month = $file.CreationTime.ToString('MM')
                if($extension.length -eq 0){
                }
                elseif($picture -like $extension){
                    write-host This file is a picture: $name $created
                    #get file created date and create a folder with the same year and month, copy file to new dir
                    New-Item -Path ${dir} -Name "pics\${year}\${month}"  -ItemType "directory" -Force
                    Copy-Item $file.FullName -Destination "${dir}pics\${year}\${month}"
                }
                elseif($extension -like $video){
                    write-host This file is a video: $name
                    New-Item -Path ${dir} -Name "vids\${year}\${month}"  -ItemType "directory" -Force
                    Copy-Item $file.FullName -Destination "${dir}vids\${year}\${month}"
                }
                elseif($extension -like $document){
                    write-host This file is a document: $name
                    New-Item -Path ${dir} -Name "docs\${year}\${month}"  -ItemType "directory" -Force
                    Copy-Item $file.FullName -Destination "${dir}docs\${year}\${month}"
                }
        }
        write-host Processing $files.Count files complete.
    }
}

function main(){
    createDir
    sortDir
}

main