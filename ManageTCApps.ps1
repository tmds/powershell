<# Instructions on running script and functions from Command line and Powershell
		Command Line: powershell -command "& { . \parentdirectory\connect.ps1; function (i.e. restart_VM, assign_VM) }"
		Powershell: . .\connect.ps1
					function (i.e. restart_VM, assign_VM)
#>

$path = "${PSScriptRoot}"

#Set execution policy to RemoteSigned.

function set_execution_policy(){
	try{
		$ExecutionPolicy = 'RemoteSigned'
		$MyExecutionPolicy = Get-ExecutionPolicy -Scope CurrentUser
		if ($MyExecutionPolicy -ne $ExecutionPolicy) { Set-ExecutionPolicy -Scope CurrentUser $ExecutionPolicy }
	}
	catch{
		write-host $Error
		return 1
	}
}

#Connects to a MS SQL database and gets the contents of a column, specifically the name column which correlates to the virtual machine names.

function get_vm_list(){
	try{
		$sql_server = "SQL_Server"
		$sql_query = "SELECT NAME FROM [SYS_ENV].[dbo].[Machines]"
		$sql_user = "SYS_ENV_ACCOUNT"
		$sql_pass = "password"

		$list = Invoke-Sqlcmd -ServerInstance $sql_server -Query $sql_query -Username $sql_user -Password $sql_pass
		return $list.Name
	}
	catch{
		write-host $Error
		return 1
	}
}

#Gets all members in an organixational unit.

function group_members($ou){
	$list = @(Get-ADGroupMember $ou -Recursive).samaccountname | ?{$_ -like "*${criteria}*"} | Sort-Object -Unique
	return $list
}

#Gets credentials to access virtual machines.

function get_credentials()
{
	try{
		$login = "domain\user"
		$pass = ConvertTo-SecureString -String "password" -AsPlainText -Force
		$creds = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $login, $pass
		return $creds
	}
	catch{
		write-host $Error
		return 1
	}
}

$cred = get_credentials

#Restarts all virtual machines.

function restart_VM(){
	try{
		set_execution_policy
		$vm_list = get_vm_list
		write-host "Restarting all VDI's..."
		Restart-Computer -Computername $vm_list -Credential $cred -Force
		return 0
	}
	catch{
		write-host $Error
		return 1
	}
}

#Assigns virtual machines randomly.

function get_random(){
	try{
		set_execution_policy
		$assigned = get_vm_list
		$random = $assigned  | Get-Random
		return $random
	}
	catch{
		write-host $Error
		return 1
	}
}

#Updates group policy on virtual machines.

function update_GP(){
	Invoke-GPUpdate
}

#Gets all profiles from virtual machines loaded or not loaded.

function get_vm_profiles_all(){
	$ErrorActionPreference = 'SilentlyContinue'
	$vm_list = get_vm_list
	$date = (get-date)
	write-host "Virtual Machine Availability for: ${date}" -f Blue
	try{
		foreach ($vm in $vm_list)
		{
			$p = Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter "LocalPath LIKE '%Users%'"
			if ($p.count -eq 0){
				write-host "There are no profiles to remove on $vm..." -F GREEN
			}
			else{
				write-host "The following profile(s) are on $vm..." -F RED
				$p.LocalPath
				$pf = $p.LocalPath -join "`n`t"
			}
		}
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Gets all profiles from virtual machines and outputs to a .csv.

function get_all_vm_profiles_csv(){
	$ErrorActionPreference = 'SilentlyContinue'
	$vm_list = get_vm_list
	$count = $vm_list.Count
	$total = 0
	$output = ""
	try{
		foreach ($vm in $vm_list)
		{
			$p = Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter "LocalPath LIKE '%Users%'"
			if ($p.count -eq 0){
				$output += "${vm},0`n"
			}
			else{
				$total += $p.count
				foreach ($profile in $p){
					$name = $profile.LocalPath
					$output += "${vm},${name}`n"
				}
			}
		}
		(get-date)| out-file -FilePath "$path\detailedreport.csv" -NoClobber -Append
		$output | out-file -FilePath "$path\detailedreport.csv" -NoClobber -Append
		write-host "${count} machines,${total} profiles"
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Gets all profiles from virtual machines.

function get_vm_profiles(){
	$ErrorActionPreference = 'SilentlyContinue'
	$vm_list = get_vm_list
			$date = (get-date)
		write-host "Virtual Machine Profiles for: ${date}" -f Blue
	try{
		foreach ($vm in $vm_list)
		{
			$p = Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter Loaded="False AND LocalPath LIKE '%Users%'"
			if ($p.count -eq 0){
				write-host "There are no profiles to remove on $vm..." -F GREEN
			}
			else{
				write-host "The following profile(s) are on $vm..." -F RED
				$p.LocalPath
				$pf = $p.LocalPath -join "`n`t"
			}
		}
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Gets all profiles from virtual machines which are NOT LOADED and outputs to a .csv.

function get_vm_profiles_csv(){
	$ErrorActionPreference = 'SilentlyContinue'
	$vm_list = get_vm_list
	$count = $vm_list.Count
	$total = 0
	$output = ""
	try{
		foreach ($vm in $vm_list)
		{
			$p = Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter Loaded="False AND LocalPath LIKE '%Users%'"
			if ($p.count -eq 0){
				$output += "${vm},0`n"
			}
			else{
				$total += $p.count
				foreach ($profile in $p){
					$name = $profile.LocalPath
					$output += "${vm},${name}`n"
				}
			}
		}
		(get-date)| out-file -FilePath "$path\detailedreport.csv" -NoClobber -Append
		$output | out-file -FilePath "$path\detailedreport.csv" -NoClobber -Append
		write-host "${count} machines,${total} profiles"
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Removes all profiles from virtual machines which are NOT LOADED.

function remove_VM_profiles(){
	$ErrorActionPreference = 'SilentlyContinue'
	$vm_list = get_vm_list
	try{
		set_execution_policy
		foreach ($vm in $vm_list)
		{
			$p = Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter Loaded="False AND LocalPath LIKE '%Users%'"
			if ($p.count -eq 0){
				write-host "There are no profiles to remove on $vm..."  -F GREEN
			}
			else{
				write-host "Removing the following profile(s) on $vm..."  -F RED
				$p.LocalPath
				$p | Remove-WMIobject
				write-host "`nAll profiles were removed from $vm."  -F RED
			}
		}
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server." -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}


#Updates printer settings to PaperSize "LetterExtra".

function update_printer_settings(){
	try{
		$vm_list = get_vm_list
		foreach ($vm in $vm_list)
		{
			#Create session to remote machine
			Invoke-Command -ComputerName $vm -ScriptBlock { Set-PrintConfiguration -PrinterName "Microsoft XPS Document Writer" -PaperSize LetterExtra } -Credential $cred
			write-host "Processing $vm..."
		}
		write-host "Processing complete. "
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Get virtual machines status with current user (samaccountname).

function get_vm_status(){
	try{
		$ErrorActionPreference = 'SilentlyContinue'
		$vm_list = get_vm_list
		#set_execution_policy
		$date = (get-date)
		write-host "Virtual Machine Availability for: ${date}" -f Blue
		foreach ($vm in $vm_list)
		{
			$sid = (Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter Loaded="True AND LocalPath LIKE '%Users%'").SID
			$time = (Get-WMIobject -Computername $vm -Credential $cred -Class Win32_UserProfile -Filter Loaded="True AND LocalPath LIKE '%Users%'").LastUseTime
				if ($sid.count -eq 0){
					 write-host "No users are on ${vm}." -f Green
				}
				else{
					$samaccountname = ([adsisearcher]"(objectsid=$sid)").FindOne().Properties.samaccountname
					write-host "${samaccountname} is using ${vm}." -f Red
				}	
		}
		return 0	
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Get all installed applications.

function get_installed_apps(){
	try{
		$ErrorActionPreference = 'SilentlyContinue'
		$vm_list = get_vm_list
		$cred = get_credentials
		foreach ($vm in $vm_list){
			write-host "Applications on ${vm}..."
			#Create session to remote machine
			Invoke-Command -ComputerName $vm -ScriptBlock {Get-ItemProperty "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*" | ?{$_.DisplayName -contains '%Microsoft%'} | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate} -Credential $cred
			write-host "Listing for ${vm} complete."
		}
		write-host "Processing complete."
		return 0
	}
	catch [System.Runtime.InteropServices.COMException]{
		write-host "The version of RPC on the remote machine does not match that of the server" -F RED
	}
	catch{
		write-host $Error
		return 1
	}
}

#Check VM status every 5 minutes

function check_vm_status(){
	do{
		get_vm_status
		Sleep 300
	}
	while($true)
}


#Check VM profile load every 5 minutes

function check_vm_profiles(){
	do{
		get_vm_profiles
		Sleep 300		
	}
	while($true)
}

#Remove all profile which are not LOADED every 3 hours.

function clean_vm_profiles(){
	do{
		remove_vm_profiles
		Sleep 10800		
	}
	while($true)
}