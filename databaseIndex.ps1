#This script finds columns that are used in joins which do not have non-clustered indexes. 
function getPotentialColumns(){
    $potentialColumns =@()
    $root = "C:\Users\Theo\Desktop\test\adventureworksdb"
    $directories = Get-ChildItem $root | Select-Object -ExpandProperty FullName

    Foreach($directory in $directories){
    $listing = Get-ChildItem $directory | Select-Object -ExpandProperty FullName
        foreach($file in $listing){
            Get-Content $file | Select-String 'join' -Context 0,1 |
                ForEach-Object{
                    $words = $_ -split " ";
                    ForEach($word in $words){
                        if($word -like "*.*"){
                            $temp =(($word.Split('.'))[1]).ToLower() | Where-Object{($null -ne $_) -OR ($_ -ne "")}
                            $temp = ((($temp -replace '[0-9~#%&*{}|:<>?/|=""\\()_;_,''\[\]-]',"").Trim()).Split("")[0]).Split()[0] 
                            $potentialColumns += $temp
                        }
                    }
                }
        }
    }
    $potentialCOlumns = $potentialColumns | Sort-Object | Get-Unique
    return $potentialColumns | Sort-Object | Get-Unique
}

function getIndexes(){
    $indexes =@()
    $root = Get-ChildItem "C:\Users\Theo\Desktop\test\adventureworksdb\table"
    $listing = Get-ChildItem $root | Select-Object -ExpandProperty FullName
    ForEach($file in $listing){
        $table = ("[" +((Get-ChildItem -Path $file).Name -replace '.sql')+"]") -replace '\.', '].['
        $temp = ((Get-Content -Path $file | Select-String 'STATISTICS' -Context 1,0).Context.PreContext-replace 'ASC').Trim()
        ForEach($item in $temp){
            if(($item.Length -gt 0) -AND ($table.Length -gt 0)){  
                $indexes += [pscustomobject]@{table=$table; index=$item}
            }
        }
    }
    return ($indexes)
}