#Author: Theo Sargeant
#Modified: September 16, 2023
#Created: April 27, 2018
#New logic for Application downtime notifications

$app_log_directory = "C:\Directory\AppDirectory\logs"
$mask = 'application.log'
$i = 0
$log_date_format = "ddd MMM d HH:mm:ss"

foreach ($log_directory in $app_log_directory)
{
    $i++

    $app_log = gci $log_directory -Filter $mask -r | Sort-Object LastWriteTime | Select-Object -Last 1 | Select-Object -ExpandProperty FullName
	$app_last_entries = gc $app_log | Select-Object -Last 3
	$app_date_time = $app_last_entries -Split " (",0, "simplematch" | where-object {$t % 2 -eq 0; $t++}
	$app_date_time = $app_date_time -Replace " - "," " | Select-Object -Last 1
	$app_last_log_time = [DateTime]::ParseExact($app_last_log_time,$log_date_format,$null)
	
	if ($app_last_entries | Select-String "Sleep Mode")
	{
		#Task
	}
}

[DateTime]::ParseExact($app_last_log_time,$log_date_format,$null)


<#
$temp = gc $path | select-object -last 3 | select-string "Sleep Mode"
$extract_date_time = $temp -split "(",0, "simplematch" | where-object {$i % 2 -eq 0; $i++}
$extract_sleep_time = $temp -split " minutes" | where-object {$i % 2 -eq 0; $i++}
$extract_sleep_time = $extract_sleep_time -split "next " | where-object {$i % 2 -eq 1; $i++}
if ($temp true)
{
"The Application has not exited Sleep mode, please contact support@company.com"
}
#>