#Author: Theo Sargeant
#Modified: March 13, 2018
#Created: September 6, 2017
#Search for a file and copy it to specified location 
#This script recursively searches a given directory for a specific file type, within that file type a query term is searched.
#The number of occurrences within that file type are then outputted with the full name of the file directory
#Example:
#	\\drive\directory\directory\mask_file.txt [In this file the work query appears. A query is a search term. Let us query.].
#
#  	Output into file as the term query appears three times in the document. The directory is listed above the count.	
#
#	\\drive\directory\directory\mask_file.txt
#	3

$directory = 'C:\Directory\AppDirectory'
$mask = '*.xml'
#(gci $directory -filter $mask -r | ?{$_.LastWriteTime.Year -ge (Get-Date).Year -1} |%{ Write-output $_.FullName (dir $_.FullName | select-string 'SEARCHTERM').count}) | out-file $env:userprofile\desktop\highlevel.log
(gci $directory -filter $mask -r | ?{$_.LastWriteTime.Year -ge (Get-Date).Year -1} | select-string 'SEARCHTERM' -context 2,4 )| out-file  -width 9999 $env:userprofile\desktop\lowlevel.log


gci $env:userprofile\oracle -filter 'log.xml' -r | copy-item -destination $env:userprofile\desktop
