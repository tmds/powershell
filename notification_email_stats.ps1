<# 
	Add dynamic Year in Format heading before foreach loop starts [November 24, 2017]
	Add condition for current year to perform analysis [November 24, 2017]
	Add logic for customer granularity [Nov 21, 2017 - Completed]
	Create new header to seperate percentage logic from granular logic [Nov 21, 2017 - Completed]
	Add logic to run for given month [November 24, 2017 Commented out in line 30]
	Create comparison between each year [TBC]
	Resolve month integers to Abbreviated month names in granular logic [February 14, 2018]
	System variable out-file path added $env:userprofile, to be run on any machine and output [March 12, 2018]
#>
function Notification(){
	$test =''
	$monthly_notify =''
	$monthly_support =''
	$test_notify =''
	$test_support =''
	$total =''
	$format =''
	$emaillist = @()
	$sender =''
	$item =''

	$path = 'C:\Directory\subdirectory'
	$date = get-date
	$time_stamp = $date.toString("MMdy")
	$year = $date.toString("yyyy")
	$num_month = get-date -uformat "%m"
	$month = (Get-Culture).DateTimeFormat.GetAbbreviatedMonthName(($date).month)
	$day = $date.toString("dd")

	Add-type -assembly "Microsoft.Office.Interop.Outlook" | out-null 
	$olFolders = "Microsoft.Office.Interop.Outlook.olDefaultFolders" -as [type]  
	$outlook = new-object -ComObject outlook.application 
	$namespace = $outlook.GetNameSpace("MAPI")
	$folder = $namespace.getDefaultFolder($olFolders::olFolderInBox) 
	
	$test_support = $folder.items | Select-Object -Property Subject, ReceivedTime, Importance, SenderName, To | ?{$_.ReceivedTime.Year -eq (Get-Date).Year}
	$test_notify = $test_support |?{$_.SenderName -like '*@*' -and $_.SenderName -notlike '* *' -and $_.SenderName -notlike '*TEST*'}
	$sender = $test_notify | Select -ExpandProperty "SenderName"

	foreach ($email in $sender){
		$emaillist+=$email.ToUpper()
	}

	$emaillist = $emaillist | Sort-Object | Get-Unique
	$monthly_support = $test_support | group-object -property{$_.ReceivedTime.Month}
	$monthly_notify = $test_notify | group-object -property{$_.ReceivedTime.Month} 
	New-Item -Type Directory -Force -Path "$path\$year\$num_month-$month"
	$format = "$month $year - Support & Notification Totals with Notify Script Percentage`n`r`n`r`n"
	$format += "Support`tNotify`tMonth`tPercentage`n`r`n"
	$format += "-------`t------`t-----`t----------`r"
	$format | out-file "$path\$year\$num_month-$month\Email_Analysis_$time_stamp.txt"
	$i = 0
	
	foreach ($total in $monthly_notify){
		$format =''
		$percent=0
		$notify=0
		$support=0
		$month_abrv = (Get-Culture).DateTimeFormat.GetAbbreviatedMonthName($monthly_notify[$i].Name)
		$notify = $monthly_notify[$i].Count
		$support = $monthly_support[$i].Count
		$percent = [math]::Round((($notify/$support)*100),2)
		$format += "$support `t$notify `t$month_abrv `t$percent%"
		$format | out-file -append "$path\$year\$num_month-$month\Email_Analysis_$time_stamp.txt"
		$i++
	}

	$dividelogic = "`r`n---------------`t`t`t`t`t---------------`r`n"
	$dividelogic | out-file -append "$path\$year\$num_month-$month\Email_Analysis_$time_stamp.txt"

	foreach ($item in $emaillist){
		$header = $item + " Total:" + ($test_notify | ?{$_.SenderName -like $item}).count + ""
		$header | out-file -append "$path\$year\$num_month-$month\Notification_Email_Analysis_$time_stamp.txt"
					
		$test_notify | ?{$_.SenderName -like $item} | group-object -property{(Get-Culture).DateTimeFormat.GetAbbreviatedMonthName($_.ReceivedTime.Month)+"`t",$_.subject} |ft -autosize count, @{label="Month`tSubject"; expression={$_.name}} | out-file -append "$path\$year\$num_month-$month\Email_Analysis_$time_stamp.txt"
	}
}
