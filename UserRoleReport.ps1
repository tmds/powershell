<#
* * * * * * * * * * * * AZMAN USER ROLE REPORT * * * * * * * * * * * *
To run locally:
	powershell -file \...\UserRoleReport.ps1 APP_ENV(APP_STORE)
	powershell -file \\eiacc96v\OPSUTIL\UserRoleReport\UserRoleReport.ps1 APP_ENV
To run from Autosys:

command: "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\powershell.exe" 
-ExecutionPolicy  Bypass -File "C:\\AUTOSYS\PROGRAM FILES
\Send-Reminders\Send-  UserRoleReport.ps1" "APP_STORE"

Revision History
20191006 - 	Added report sending as an attachment to the Operations Team
20191014 - 	Added second report format with roles listed per user with reporting manager
			Added second report sending as an attachment to Support

#>

filter timestamp {"$(Get-Date -Format G): $_"}
$ErrorActionPreference = "Stop"
#$ErrorView = "NormalView"

function format_date(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$format = ""
		$year = (get-date).toString("yyyy")
		$month =(get-date).toString("MM")
		$day =(get-date).toString("dd")
		$hour=(get-date).toString("hh")
		$format = "${year}${month}${day}${hour}"
		return $format
	}
	catch{
		write-host $Error
		return 1
		exit	
	}
}

function log_stamp(){
	try{
		$date = Get-Date
		$year = $date.ToString("yyyy")
		$month = $date.ToString("MM")
		$day = $date.ToString("dd")
		$path = "${PSScriptRoot}`\Logs`\${year}`\${month}"
		New-Item -Path $path -force -Name "$day" -ItemType "Directory" | Out-Null
		return $path += "`\$day`\RunTime.log"
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		$Error | timestamp
		Exit 1
	}
}

function log_stamp2(){
	try{
		$date = Get-Date
		$year = $date.ToString("yyyy")
		$month = $date.ToString("MM")
		$day = $date.ToString("dd")
		$path = "${PSScriptRoot}`\Logs`\${year}`\${month}"
		New-Item -Path $path -force -Name "$day" -ItemType "Directory" | Out-Null
		return $path += "`\$day`\Error.log"
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		$Error
		Exit 1
	}
}

$stamp = timestamp
$log_stamp = log_stamp
$log_stamp2 = log_stamp2
$format = format_date

<#
This function takes in the user input and determines whether it is valid
or not, if the input does not contain a delimiting '_' the input is not
valid and the user is informed with proper formatting provided in an
example. If the input is valid the application is returned.
	e.g. 	input:APP_STORE
			output:APP
#>

function get_app($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$app = ""
	$example = "`n`n`te.g. 'UserRoleReport.ps1 APP_STORE'"
	
	try{
		if ($app_store.contains("_")){
			$app = $app_store.substring(0,$app_store.IndexOf('_'))
			write-host $app
		}
		else{
			write-host "`nThe application/environment format entered is invalid, try again.${example}"
			exit
		}
	return $app
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
This function takes in the user input and determines whether it is valid
or not, if the input does not contain a delimiting '_' the input is not
valid and the user is informed with proper formatting provided in an
example. If the input is valid the environment is returned.
	e.g. 	input:APP_STORE
			output:STORE
#>

function get_env($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$env = ""
	$example = "`n`n`te.g. 'UserRoleReport.ps1 APP_STORE'"

	try{
		if ($app_store.contains("_")){
			$env = $app_store.substring($app_store.IndexOf('_')+1)
			write-host $env
		}
		else{
			write-host "`nThe application/environment format entered is invalid, try again.${example}"
			exit
		}
		return $env
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript
}

<#
This function formats the date for the .csv output appropriately.
	e.g. 	input:
			output: APP2_PROD 1728 2019 09 17 57 User Role Report
#>


<#
This function gets the correct (most recent match) .xml file based on the
specified application and environment entered by the user. The most 
recent .xml file for the specified application and environment is
returned.
	e.g. 	input:APP_STORE
			output:APP_STORE.xml
#>
function get_xml_file($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$env = get_env $app_store
		$path ="\\server\PATH"
		write-host $path
		$file = Get-ChildItem $path | Where-Object{$_.Name -like "*$app_store*" -and $_.Extension -eq ".xml"} | Sort-Object LastWriteTime | Select-Object -last 1
		$stamp = "The following file was used to generate this report for ${app_store}:, ${path}${file}"
		write-host $stamp
		$stamp | out-file "\\server\PATH\${app}_${env}${format}UserRoleReport.csv" -Encoding ASCII
		[xml]$xml = get-content "$path$file"
		return $xml
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
This function accomodates different xml structure by switching context
depending on the application specified and returns the correct
parent node for XPath traversal.
	e.g. 	input:APP
			output:applicationGroups/applicationGroup/user
#>
function get_parent_node($app){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
    try{
		if($app -like '*APP2*')
		{	#APP2
			$parent_node = "roleAssignments/role/user"
		}
		else 
		{	#APP, APP3, APP4, APP5, APP6 
			$parent_node = "applicationGroups/applicationGroup/user"
		}
		return $parent_node
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
This function sets the header for the .csv output appropriately.
	e.g. 	input:APP STORE
			output: \\server\PATH\UserRoleReport\APPUserRoleReport.csv
#>
function set_csv_header($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$header = "User Id,Name,Role`n"
		$header | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8 -append
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
This function sets the header for the .csv output appropriately.
	e.g. 	input:APP STORE
			output: \\server\PATH\UserRoleReport\APPUserRoleReport.csv
#>
function set_APP5_header($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$header = "User Id,Name,Access,Description`n"
		$header | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -append
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
This function gets the count child nodes in the parent node which are
used in for loop iteration.
	e.g. 	input:xml parent node
			output: 99
#>
function get_count($xml){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$count = $xml.selectnodes("applicationStoreDefinitions/${parent_node}").count
		return $count
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
The following function creates a database connection to APP4 STORE and
retrieves the table containing the security group and role information
and outputs the data into a .csv file.
#>
function get_APP3_STORE($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$sql_server = "SQL-Server"
		$sql_query = "SELECT t999SGNetworkUserName AS 'User_Id', t999AccessSecurityGroup AS 'Security_Group', t998.t998Decode AS 'Role'
		FROM seSecurityAccessT999 t999 JOIN [seSecurityAccessLevelT998] t998 ON t999.t999AccessLevel = t998.t998Code
		ORDER BY t999SGNetworkUserName"	
		$sql_user = "APP4_User"
		$sql_pass = "password"

		$roles = Invoke-Sqlcmd -ServerInstance $sql_server -Query $sql_query -Username $sql_user -Password $sql_pass
		$stamp = "The following report was generated with ${app_store} database."
		write-host $stamp
		$stamp | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8
		set_csv_header $app $env

			for($i=0; $i -lt $roles.count; $i++)
			{
				$user = $roles.User_Id[$i]
				$security = $roles.Security_Group[$i]
				$user_role = $roles.Role[$i]
				$row = "${user},${security},${user_role}"

				$row | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv"  -Encoding UTF8 -APPEND
			}
		return $roles
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
The following function creates a database connection to APP4 PROD and
retrieves the table containing the security group and role information
and outputs the data into a .csv file.
#>
function get_APP4_PROD($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$sql_server = "C-domain-SQLC67A"
		$sql_query = "SELECT t999SGNetworkUserName as 'User_Id', t999AccessSecurityGroup 'Security_Group', t998.t998Decode as 'Role'
		FROM seSecurityAccessT999 t999 JOIN [seSecurityAccessLevelT998] t998 ON t999.t999AccessLevel = t998.t998Code
		ORDER BY t999SGNetworkUserName"		
		$sql_user = "APP4_User"
		$sql_pass = "password"

		$roles = Invoke-Sqlcmd -ServerInstance $sql_server -Query $sql_query -Username $sql_user -Password $sql_pass
		$stamp = "The following report was generated with ${app_store} database."
		write-host $stamp
		$stamp | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8
		set_APP5_header $app $env

			for($i=0; $i -lt $roles.count; $i++)
			{
				$user = $roles.User_Id[$i]
				$security = $roles.Security_Group[$i]
				$user_role = $roles.Role[$i]
				$row = "${user},${security},${user_role}"	
				$row | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8 -APPEND
			}
		return $roles
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
The following function creates a database connection to APP4 STORE and
retrieves the table containint the security group and role information
and outputs the data into a .csv file.
#>
function get_APP4_STORE($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$sql_server = "SQL-Server"
		$sql_query = "SELECT        seSecurityAccessT999.t999NetworkUserName 'User_ID', seSecurityGroupT9998.t9998NetworkUserFullName 'Name', seSecurityAccessLevelT998.t998Decode Access, 
								  seSecurityApplicationT995.t995FunctionDescription Description
		FROM            seSecurityAccessT999 INNER JOIN
								 seSecurityAccessLevelT998 ON seSecurityAccessT999.t999AccessLevelCode = seSecurityAccessLevelT998.t998Code INNER JOIN
								 seSecurityGroupT9998 ON seSecurityAccessT999.t999NetworkUserName = seSecurityGroupT9998.t9998NetworkUserName INNER JOIN
								 seSecurityApplicationT995 ON seSecurityAccessT999.t999FunctionId = seSecurityApplicationT995.t995FunctionId"	
		$sql_user = "APP4_User"
		$sql_pass = "password"

		$roles = Invoke-Sqlcmd -ServerInstance $sql_server -Query $sql_query -Username $sql_user -Password $sql_pass
		$stamp = "The following report was generated with ${app_store} database."
		write-host $stamp
		$stamp | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8
		set_APP5_header $app $env

			for($i=0; $i -lt $roles.count; $i++)
			{
				$user = $roles.User_Id[$i]
				$name = $roles.Name[$i]
				$access = $roles.Access[$i]
				$description = $roles.Description[$i]
				$row = "${user},${name},${access},${description}"

				$row | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8 -APPEND
			}
		return $roles
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
The following function creates a database connection to APP4 PROD and
retrieves the table containint the security group and role information
and outputs the data into a .csv file.
#>
function get_APP4_PROD($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$sql_server = "EIPDB90S"
		$sql_query = "SELECT        seSecurityAccessT999.t999NetworkUserName 'User_ID', seSecurityGroupT9998.t9998NetworkUserFullName Name, seSecurityAccessLevelT998.t998Decode Access, 
								  seSecurityApplicationT995.t995FunctionDescription Description
		FROM            seSecurityAccessT999 INNER JOIN
								 seSecurityAccessLevelT998 ON seSecurityAccessT999.t999AccessLevelCode = seSecurityAccessLevelT998.t998Code INNER JOIN
								 seSecurityGroupT9998 ON seSecurityAccessT999.t999NetworkUserName = seSecurityGroupT9998.t9998NetworkUserName INNER JOIN
								 seSecurityApplicationT995 ON seSecurityAccessT999.t999FunctionId = seSecurityApplicationT995.t995FunctionId"		
		$sql_user = "APP4_User"
		$sql_pass = "password"
		$stamp = "The following report was generated with ${app_store} database."
		write-host $stamp
		$roles = Invoke-Sqlcmd -ServerInstance $sql_server -Query $sql_query -Username $sql_user -Password $sql_pass

		$stamp | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Encoding UTF8
		set_APP5_header $app $env

			for($i=0; $i -lt $roles.count; $i++)
			{
				$user = $roles.User_Id[$i]
				$name = $roles.Name[$i]
				$access = $roles.Access[$i]
				$description = $roles.Description[$i]
				$row = "${user},${name},${access},${description}"

				$row | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv"  -Encoding UTF8 -APPEND
			}
		return $roles
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
This function takes in the specified .xml file along with the
application and environment. A substring operation is performed on the 
name variable to extract the username. The role element from the .xml
file is pulled for each name. All three varaibles are then formatted into
a row variable which is comma delimited and then outputted to the
specified .csv file.
	e.g. 	input: first.last@domain xml_variable STORE APP
			output: "first.last,first.last@domain,role(Administrator)"
#>
function generate_row($name, $xml, $env, $app){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		if($name -like "*S-1-5*"){
			$username =""
			$name =""
			$role =""
			$row = "$username,$name,$role"
		}
		elseif($name -like "*domain\*"){#APP6
			$username = $name.substring($name.indexof('\')+1)
			$role = $xml.selectnodes("applicationStoreDefinitions/${parent_node}").parentnode.name[$i]
			$row = "$username,$name,$role"
			$row | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv"  -Encoding ASCII -APPEND
		}
		else{
			$username = $name.substring(0,$name.indexof('@'))
			$role = $xml.selectnodes("applicationStoreDefinitions/${parent_node}").parentnode.name[$i]
			$row = "$username,$name,$role"
			$row | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv"  -Encoding ASCII -APPEND
		}
		return $row
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
Checks to see if the environment is valid, then checks to see if the
application backup .xml exists in the environment DATA directory
	e.g. 	input: APP_STORE APP STORE
			output: true/false
#>
function check_app_store($app_store, $app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		if ($env -eq "PROD" -or $env -eq "STORE"){
			$path ="\\userpref_${env}_application\applctn\ASM\${env}\DATA\"
			$file_list = Get-ChildItem $path
			$file_list += "APP4_STORE","APP4_PROD", "APP4_STORE", "APP4_PROD"
			$example = "`n`n`te.g. 'UserRoleReport.ps1 APP_STORE'"
			if($file_list -like "*$app_store*"){
				write-host "`nProcessing UserRoleReport for ${app_store}..."
				return 0
			}
			else{
				write-host "`nThe application entered is invalid, try again.${example}"
				return 1
			}
		}
		else{
			write-host "`nThe environment entered is invalid, try again.${example}"
			return 1
		}
	}
	catch{
		write-host $Error
		return 1
		exit
	}
	Stop-Transcript	
}

<#
Generates the user role report using the $app_store entered by the
user. The value entered by the user is verifed to be valid, if it is
valid a User Role Report will be generates for the specified 
application and specified environment. This function outputs a .csv
file with the contents of the user role report to the specified 
environment in the following directory depending on the input sample
directory:
	e.g. 	input: APP_STORE
			output:\\server\PATH\UserRoleReport\
		    APPUserRoleReport.csv
#>
function generate_user_role_report($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$app = get_app $app_store
		$env = get_env $app_store
		$valid = check_app_store $app_store $app $env
		if ($valid -eq 0){
			if($app -eq 'APP4' -or $app -eq 'APP4'){
				switch($app_store.ToUpper()){
				"APP4_STORE"{get_APP4_STORE $app $env}
				"APP4_PROD"{get_APP4_PROD $app $env}
				"APP4_STORE"{get_APP4_STORE $app $env}
				"APP4_PROD"{get_APP4_PROD $app $env}
				}
			}
			else{
				[xml]$xml = get_xml_file $app_store
				$parent_node = get_parent_node $app
				set_csv_header $app $env
				$count = get_count $xml
			
				for($i=0; $i -lt $count; $i++)
				{
					$name = $xml.selectnodes("applicationStoreDefinitions/${parent_node}").name[$i]
					generate_row $name $xml $env $app
				}
			}
		write-host "`nProcessing UserRoleReport for ${app_store} complete."	
		}
		else{
			exit
		}
	}
	catch{
		write-host $Error
		return 0
		exit
	}
	Stop-Transcript
}

<#

<#
These are the helper functions to send the email with attachment to support
#>

function get_smtp_server(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$smtp_server = "192.168.0.1"
		Write-Host ("SMTP Server:" | timestamp) -f Green
		Write-Host ($smtp_server | timestamp) -f Green
		return $smtp_server
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		Exit 1
	}
	Stop-Transcript
}

function get_mail_credentials(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$login = "domain\support"
		$pass = ConvertTo-SecureString -String "password" -AsPlainText -Force
		$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $login, $pass
		Write-Host ("Mail credentials are valid." | timestamp) -f Green
		return $cred
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		$Error[0] | Select-Object * | timestamp  | Out-String | Out-File "$log_stamp2" -Append
		Exit 1
	}
	Stop-Transcript
}

function get_sender(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$_sender = "Support <support@domain>"	
	return $_sender
	Stop-Transcript	
}

function get_to(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$to = "Support <support@domain>"
	return $to
	Stop-Transcript	
}

function get_cc(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$cc = "Support <support@domain>"	
	return $cc
	Stop-Transcript	
}

function get_year(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$year = (get-date).toString("yyyy")
	Write-Host ("Year:" | timestamp) -f Green
	Write-Host ($year | timestamp) -f Green
	return $year
	Stop-Transcript	
}

function get_month(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$month = (get-date).toString("MMMM")
	Write-Host ("Month:" | timestamp) -f Green
	Write-Host ($month | timestamp) -f Green
	return $month
	Stop-Transcript	
}

function get_day(){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$day =(get-date).toString("dd")
	Write-Host ("Day:" | timestamp) -f Green
	Write-Host ($day | timestamp) -f Green
	return $day
	Stop-Transcript	
}

function get_subject($app, $env, $month, $day, $year){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$subject = "${app} ${env} - User Role Report ${month} ${day}, ${year}"	
	return $subject
	Stop-Transcript	
}

function get_email_body($app, $env){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	$email_body = "The User Role Report for ${app} ${env} has been generated successfully.`r`n"	
	$email_body += "Attached is the report.`r`n"
	$email_body += "Any data integrity concerns should be directed to Support <support@domain.com>.`r`n"
	$email_body += "Thank you,`rSupport"	
	return $email_body
	Stop-Transcript
}

<#User Name : user@domain       Report To: John.Doe
-------------------------------------------------------
 
AzMan Roles: 
Application Group : Application_Production_Support
#>

function get_csv($app_store)
{
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$app = get_app $app_store
		$env = get_env $app_store
		$csv = Import-CSV "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv"
		$id_list = $csv."Name" | sort-object -Unique
		return $id_list
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		$Error[0] | Select-Object * | timestamp  | Out-String | Out-File "$log_stamp2" -Append
		Exit 1
	}
	Stop-Transcript | timestamp
}

function generate_second_report($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$app = get_app $app_store
		$env = get_env $app_store
		$csv = Import-CSV "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv" -Header "User Id","Name","Role" | Select-Object -Skip 2
		$stamp = "The following report was generated for ${app_store} \\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv.`n"
		$stamp | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt"
		$id_list = $csv."User Id" | sort-object -Unique
		
		foreach($id in $id_list)
		{
		$role_list = (($csv | Sort-Object "User Id", "Role" -Unique | Where-Object {$_.Name -like "*$id*"}).Role)
		$manager = ([adsisearcher]"(samaccountname=$id)").FindOne().Properties.extensionattribute7
		"------------------------------------------------------------------------------" | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt"-APPEND
		"User Name: $id`t`t Reports To: $manager" | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt" -APPEND
		"------------------------------------------------------------------------------`n" | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt"-APPEND
		"${app} ${env} AzMan Roles:`n" | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt"-APPEND
			foreach ($role in $role_list){
			"`tApplication Group: $role" | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt" -APPEND
			}
			"" | out-file "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt" -APPEND
		}
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		$Error[0] | Select-Object * | timestamp | Out-File "$log_stamp2" -Append
		Exit 1
	}
	Stop-Transcript
}

<#
This function sends an email to support with the UserRoleReport attached
in .csv file format.
#>

function send_mail_report($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	try{
		$app = get_app $app_store
		$env = get_env $app_store
		$cred = get_mail_credentials
		$path = @("\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.csv")
		$path2 = "\\server\PATH\UserRoleReport\${app}_${env}${format}UserRoleReport.txt"
		$path += $path2
		$month = get_month
		$day = get_day
		$year = get_year	
		$email_to = get_to
		#$email_cc = get_cc
		$email_from = get_sender
		$email_subject = get_subject $app $env $month $day $year
		$SMTP_Email_Server = get_smtp_server
		$email_body = get_email_body $app $env
		write-host $email_body
		Send-MailMessage -From $email_from -To $email_to -Subject $email_subject -Body $email_body -SmtpServer $SMTP_Email_Server -Attachments $path -Credential $cred
		Write-Host ("Complete." | timestamp) -f Green
		Exit 0
	}
	catch{
		Write-Host ($Error[0] | timestamp) -f Red
		$Error[0] | Select-Object * | timestamp  | Out-String | Out-File "$log_stamp2" -Append
		Exit 1
	}
	Stop-Transcript
}
 
<#
This function runs the UserRoleReport and sends an email to support if the
UserRoleReport is generated successfully.
#>

function run_report($app_store){
	Start-Transcript -Path "$log_stamp" -NoClobber -IncludeInvocationHeader -Append | Out-Null
	if((generate_user_role_report $app_store)){
		if((generate_second_report $app_store)){
			send_mail_report $app_store
			exit 0
		}
	}
	else{
		write-host $Error[0]
		exit 1
	}
	Stop-Transcript
}
<#
User input is the one and only parameter required to run this script
	e.g. 	input: APP_STORE
#>

$_input=$args[0]
run_report $_input
