<#Author: Theo Sargeant
#Created: September 6, 2017
#Modified: September 16, 2023 
#This script recursively searches a given directory for a specific file type, within that file type a query term is searched.
#The number of occurrences within that file type are then outputted with the full name of the file directory
#Example:
#	\\drive\directory\directory\mask_file.txt [In this file the work query appears. A query is a search term. Let us query.].
#
#  	Output into file as the term query appears three times in the document. The directory is listed above the count.	
#
#	\\drive\directory\directory\mask_file.txt
#>

function FindApplicationErrors{	
    $region_list = @()
    $pdf_list = @()
    $directory = 'c:\Directory\AppDirectory'
    $appmask = 'application.log'

    $year = (get-date).year
    $num_month = get-date -uformat "%m"
    $month = (Get-Culture).DateTimeFormat.GetAbbreviatedMonthName((get-date).month)

    $regional = (gci $directory -filter $appmask -r | ?{$_.LastWriteTime.Year -ge (Get-Date).Year -and $_.LastWriteTime.Month -ge (Get-Date).Month -3} | select-string 'Error loading cold data' -context 1,0 )
    $pdfinstall = (gci $directory -filter $appmask -r | ?{$_.LastWriteTime.Year -ge (Get-Date).Year -and $_.LastWriteTime.Month -ge (Get-Date).Month -3} | select-string 'Error trying to Send Email' -context 1,0 )

    #region application error user list
    $regional_context = $regional | select-object -expandproperty context | select-object -expandproperty precontext 
    $regional_context = $regional_context -split " Process" | where-object {$i % 2 -eq 0; $i++}
    $regional_context = $regional_context -split "ID: " | where-object {$i % 2 -eq 1; $i++} | sort-object | get-unique

    #pdfinstall application error user list
    $pdfinstall_context = $pdfinstall | select-object -expandproperty context | select-object -expandproperty precontext
    $pdfinstall_context = $pdfinstall_context -split " Process" | where-object {$i % 2 -eq 0; $i++}
    $pdfinstall_context = $pdfinstall_context -split "ID: " | where-object {$i % 2 -eq 1; $i++} | sort-object | get-unique

    foreach($region in $regional_context ){
        $region = $region.ToUpper()
        $region = "`t$region`n`r`n"
        $region_list += $region
    }

    foreach($pdf in $pdfinstall_context ){
        $pdf = $pdf.ToUpper()
        $pdf = "`t$pdf`n`r`n"
        $pdf_list += $pdf
    }

    $region_header = "The Regional Settings for the following user(s) is not in the expected format (M/d/yyyy):`n`r`n`r`n$region_list" | out-file "$directory\logs\$year\$num_month-$month\AppFixes.log"
    $pdf_header = "The PDF Converter ($directory\PDFInstall.exe) is has not been installed for the following user(s):`n`r`n`r`n$pdf_list" | out-file -append "$directory\logs\$year\$num_month-$month\AppFixes.log"
}

FindApplicationErrors